import pycurl
from StringIO import StringIO
from bs4 import BeautifulSoup
import json
import urllib
import ipdb


def parse_and_save_img(body, url_counter = 0):
    soup  = BeautifulSoup(body)
    imgs= soup.select('img')
    counter = 0
    for img in imgs:
        data = json.loads(img['data-img-config'])
        url = data['base_path'] + data['320']
        urllib.urlretrieve(url, '/root/crawl/img/' + url_counter + '_' + str(counter) + '.jpg')
        counter += 1

def get_url_data(start):
    buffer = StringIO()
    url = 'http://www.jabong.com/men/clothing/shirts/formal-shirts/?source=topnav_men&ax=1&page='+start+'&limit=52&sortField=popularity&sortBy=desc'
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()
    body = buffer.getvalue()
    return body

buffer = StringIO()
for i in range(1, 76):
    current = str(i)
    body = get_url_data(current)
    parse_and_save_img(body, current)
