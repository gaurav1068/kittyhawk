import numpy as np
import cv2
import copy

UPLOAD_FOLDER = '/var/www/image/raw_image'

def detect_shirt_color(img):
    j = 130
    x = img[j, 100][0]
    y = img[j, 100][1]
    z = img[j, 100][2]

    while (x == 0 and y == 0 and z == 0):
        j = j-1
        x = img[j, 100][0]
        y = img[j, 100][1]
        z = img[j, 100][2]

    return copy.copy(img[j, 100])


def remove_extra(img, pant):
    threshold = 25
    for i in range(0, 272):
        for j in range(0, 200):
            if -1 * threshold <= (int(img[i, j][0]) - int(pant[0])) <= threshold:
                if -1 * threshold <= (int(img[i, j][1]) - int(pant[1])) <= threshold:
                    if -1 * threshold <= (int(img[i, j][2]) - int(pant[2])) <= threshold:
                        # img[i, j] = [0, 0, 0]
                        img[i, j] = img[i, j]
                    else:
                        img[i, j] = [0, 0, 0]
                else:
                    img[i, j] = [0, 0, 0]
            else:
                img[i, j] = [0, 0, 0]
    return img

def convert_file(path):
    print 'started'
    save_path = UPLOAD_FOLDER + '/converted.jpg'
    print 'started'
    print path
    img = cv2.imread(path)
    mask = np.zeros(img.shape[:2], np.uint8)
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)
    print 'started mid'

    rect = (5, 5, 450, 290)
    cv2.grabCut(img, mask, rect, bgdModel,
    	    fgdModel, 5, cv2.GC_INIT_WITH_RECT)

    print 'started cut finish'
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
    img = img*mask2[:, :, np.newaxis]

    pant = detect_shirt_color(img)
    img = remove_extra(img, pant)
    cv2.imwrite(save_path, img)
